all: compile build
compile:
	g++ *.cpp -c -I/home/user/src
build:
	g++ -rdynamic *.o zlib/libz_bundled.a snappy/libsnappy_bundled.a -lX11 -lpthread -ldl -lrt -lproc -o retrace
clean:
	rm -f *.o retrace
